package edu.ulyanova.kmeans;

import com.opencsv.CSVReader;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * K-means clustering algorithm
 * https://en.wikipedia.org/wiki/K-means_clustering
 */
public class App {
    public static void main(String[] args) {
        if (args.length < 2) {
            throw new IllegalArgumentException("Data file path and cluster amount are needed: example \"data/data.csv 3\"");
        }
        String csvFileName = args[0];
        int clusters = Integer.parseInt(args[1]);

        double[][] data = getData(csvFileName);

        KMeansAlgorithm kMeansAlgorithm = new KMeansAlgorithm(clusters, data);
        Map<Integer, Set<Integer>> kMeansClusteringResult = kMeansAlgorithm.doClustering();
        System.out.println("Result: " + kMeansClusteringResult.toString());

    }

    private static double[][] getData(String csvFileName) {
        File csvFile = new File(csvFileName);
        List<String[]> dataLines = new LinkedList<>();
        try (CSVReader reader = new CSVReader(new FileReader(csvFile), ';', '\"', 1)) {
            String[] line;
            while ((line = reader.readNext()) != null) {
                dataLines.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        int observationsAmount = dataLines.size();
        int vectorDimension = dataLines.iterator().next().length;

        double[][] data = new double[observationsAmount][vectorDimension];
        Iterator<String[]> iterator = dataLines.iterator();
        int index = 0;
        while (iterator.hasNext()) {
            String[] line = iterator.next();
            for (int i = 0; i < vectorDimension; i++) {
                data[index][i] = Double.parseDouble(line[i]);
            }
            index++;
        }
        return data;
    }
}
