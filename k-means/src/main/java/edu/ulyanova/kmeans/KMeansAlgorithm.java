package edu.ulyanova.kmeans;

import java.util.*;

public class KMeansAlgorithm {
    private final double THRESHOLD = .0001;
    private final int clustersAmount;
    private final int observationsAmount;
    private final int vectorDimension;
    private final double[][] observations;


    public KMeansAlgorithm(int clustersAmount, double[][] observations) {
        this.clustersAmount = clustersAmount;
        this.observations = observations;
        this.observationsAmount = observations.length;
        this.vectorDimension = observations[0].length;
    }

    public Map<Integer, Set<Integer>> doClustering() {
        //init centroids with first k observations
        double[][] centroids = new double[clustersAmount][vectorDimension];
        for (int i = 0; i < clustersAmount; i++) {
            centroids[i] = Arrays.copyOf(observations[i], vectorDimension);
        }

        Map<Integer, Set<Integer>> clusteringResult = createNewClusteringResult();

        //recalculate centroids starting from 0+clustersAmount, skipping first observations
        for (int observationIndex = clustersAmount; observationIndex < observationsAmount; observationIndex++) {
            int nearestMeanIndex = findNearestMean(centroids, observations[observationIndex]);
            recalculateMean(centroids[nearestMeanIndex], observations[observationIndex]);
        }

        boolean clustersChanged = true;
        while (clustersChanged) {
            for (int index = 0; index < observationsAmount; index++) {
                int nearestMeanIndex = findNearestMean(centroids, observations[index]);
                recalculateMean(centroids[nearestMeanIndex], observations[index]);
            }
            Map<Integer, Set<Integer>> newClusteringResult = doClustering(centroids);

            if (clusteringResult.equals(newClusteringResult)) {
                clustersChanged = false;
            }
            clusteringResult = newClusteringResult;
        }

        return clusteringResult;
    }

    /**
     * Calculates the Euclidean distance,
     * this is intuitively the "nearest" mean
     *
     * @return index of nearest centroid
     */
    private int findNearestMean(double[][] centroids, double[] observation) {
        double minimalDistance = Double.MAX_VALUE;
        int minimalMeanIndex = Integer.MAX_VALUE;
        double currentDistance = 0;

        for (int meanIndex = 0; meanIndex < clustersAmount; meanIndex++) {
            double sumOfSquares = 0;
            for (int i = 0; i < vectorDimension; i++) {
                sumOfSquares += Math.pow(observation[i] - centroids[meanIndex][i], 2);
            }
            currentDistance = Math.sqrt(sumOfSquares);
            if (minimalDistance - currentDistance > THRESHOLD) {
                minimalDistance = currentDistance;
                minimalMeanIndex = meanIndex;
            }
        }

        return minimalMeanIndex;
    }

    private void recalculateMean(double[] mean, double[] observation) {
        for (int i = 0; i < vectorDimension; i++) {
            mean[i] = (observation[i] + mean[i]) / 2;
        }
    }

    private Map<Integer, Set<Integer>> doClustering(double[][] means) {
        Map<Integer, Set<Integer>> result = createNewClusteringResult();

        for (int index = 0; index < observationsAmount; index++) {
            result.get(findNearestMean(means, observations[index])).add(index);
        }

        return result;
    }

    private Map<Integer, Set<Integer>> createNewClusteringResult() {
        Map<Integer, Set<Integer>> result = new HashMap<>(clustersAmount);
        for (int i = 0; i < clustersAmount; i++) {
            result.put(i, new HashSet<Integer>());
        }
        return result;
    }
}
