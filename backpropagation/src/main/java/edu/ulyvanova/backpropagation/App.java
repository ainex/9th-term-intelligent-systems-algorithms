package edu.ulyvanova.backpropagation;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Arrays;

/**
 * https://en.wikipedia.org/wiki/Multilayer_perceptron
 * Learning occurs in the perceptron by changing connection weights after each piece of data is processed,
 * based on the amount of error in the output compared to the expected result.
 * This is an example of supervised learning, and is carried out through backpropagation,
 * a generalization of the least mean squares algorithm in the linear perceptron
 */
public class App {
    private static final double THRESHOLD = 0.01;
    public static final double EXPECTED_OUTPUT = 0.4;
    public static final int SCALE = 5;

    public static void main(String[] args) {

        //matrix representation of the perceptron
        double[][] W = {
                {0, 0, -1, 1, 3, 0},
                {0, 0, 2, 1, -2, 0},
                {0, 0, 0, 0, 0, 1},
                {0, 0, 0, 0, 0, 2},
                {0, 0, 0, 0, 0, 4},
                {0, 0, 0, 0, 0, 0},
        };
        int dimension = W.length;
        System.out.println("Backpropagation demo for preceptron:");
        printMatrix(dimension, W);

        int outputNeuronIndex = dimension - 1;
        System.out.println("Expected output: " + EXPECTED_OUTPUT + " on neuron " + outputNeuronIndex);

        double[] activities = new double[dimension];
        activities[0] = 0.2; //input
        activities[1] = 0.5; //input
        calculateActivities(W, activities);

        boolean needCorrection = true;
        int correctionCount = 0;
        while (needCorrection) {
            correctionCount++;
            double[] errors = calculateErrors(W, outputNeuronIndex, activities, dimension);

            double[][] deltaW = calculateDeltaW(W, activities, errors, dimension);
            // printMatrix(dimension, deltaW);

            // apply delta to W matrix
            applyDeltaToW(W, deltaW, dimension);
            //printMatrix(dimension, W);

            calculateActivities(W, activities);
            if (Math.abs(EXPECTED_OUTPUT - activities[outputNeuronIndex]) < THRESHOLD) {
                needCorrection = false;
            }
        }
        System.out.println("\nResults: \nActivities: " + Arrays.toString(activities));
        System.out.println("Corrected matrix W: ");
        printMatrix(dimension, W);
        System.out.println("Corrections amount: " + correctionCount);

    }

    /**
     * errorj = errork * wkj * f(netj) * (1 - f(netj))
     */
    private static double[] calculateErrors(double[][] w, int outputNeuronIndex, double[] activities, int dimension) {
        double[] errors = new double[dimension];
        //error calculation for output neuron
        errors[outputNeuronIndex] = (EXPECTED_OUTPUT - activities[outputNeuronIndex]) * activities[outputNeuronIndex] * (1 - activities[outputNeuronIndex]);
        //error calculation for inner layer
        for (int i = 4; i >= 2; i--) {
            errors[i] = w[i][outputNeuronIndex] * errors[outputNeuronIndex] * activities[i] * (1 - activities[i]);
        }
        return errors;
    }

    private static double[][] calculateDeltaW(double[][] w, double[] activities, double[] errors, int dimension) {
        double[][] deltaW = new double[dimension][dimension];
        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                if (w[i][j] != 0) {
                    double delta = activities[i] * errors[j] * 0.85;
                    deltaW[i][j] = BigDecimal.valueOf(delta).setScale(SCALE, RoundingMode.HALF_UP).doubleValue();
                    //deltaW[i][j] = activities[i] * errors[j] * 0.85;
                }
            }
        }
        return deltaW;
    }

    private static void applyDeltaToW(double[][] w, double[][] deltaW, int dimension) {
        for (int i = 0; i < dimension; i++) {
            for (int j = 0; j < dimension; j++) {
                if (w[i][j] != 0) {
                    w[i][j] = BigDecimal.valueOf(w[i][j] + deltaW[i][j]).setScale(SCALE, RoundingMode.HALF_UP).doubleValue();
                }
            }
        }
    }

    private static void printMatrix(int dimension, double[][] deltaW) {
        for (int i = 0; i < dimension; i++) {
            System.out.println(Arrays.toString(deltaW[i]));
        }
    }

    private static void calculateActivities(double[][] W, double[] activities) {
        //skip first 0 and 1 neuron, as they are input neurons
        for (int j = 2; j < W[0].length; j++) {
            double input = 0;
            for (int i = 0; i < j; i++) {
                input += W[i][j] * activities[i];
            }
            activities[j] = activationFunction(input);
        }
    }

    /**
     * https://en.wikipedia.org/wiki/Logistic_function
     * A logistic function or logistic curve is a common "S" shape (sigmoid curve), with equation:
     * f(x) = 1 / (1 + e^-x)
     *
     * @return f(x)
     */
    private static double activationFunction(double x) {
        return 1 / (1 + Math.exp(-1 * x));
    }
}
