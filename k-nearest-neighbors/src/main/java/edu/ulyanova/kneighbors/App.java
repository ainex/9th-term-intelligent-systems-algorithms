package edu.ulyanova.kneighbors;

import com.opencsv.CSVReader;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * k-nearest neighbors algorithm
 * https://en.wikipedia.org/wiki/K-nearest_neighbors_algorithm
 */
public class App {
    public static void main(String[] args) {
        if (args.length < 3) {
            throw new IllegalArgumentException("Training set file, data set file paths and k-amounts are needed: example \"data/test-data.csv data/data.csv 3\"");
        }
        String csvTrainingSet = args[0];
        String csvTestSet = args[1];
        int k = Integer.parseInt(args[2]);

        DataWithClasses dataWithClasses = getDataWithClasses(csvTrainingSet);
        double[][] data = getData(csvTestSet);


        KNearestNeighborsAlgorithm algorithm = new KNearestNeighborsAlgorithm(dataWithClasses.data, dataWithClasses.classLabels, data, k);
        int[] testClassLabels = algorithm.doClassification();
        System.out.println("Result: " + Arrays.toString(testClassLabels));

    }

    private static double[][] getData(String csvFileName) {
        List<String[]> dataLines = parseToLines(csvFileName);
        int dataSize = dataLines.size();
        int vectorDimension = dataLines.iterator().next().length;

        double[][] data = new double[dataSize][vectorDimension];
        Iterator<String[]> iterator = dataLines.iterator();
        int index = 0;
        while (iterator.hasNext()) {
            String[] line = iterator.next();
            for (int i = 0; i < vectorDimension; i++) {
                data[index][i] = Double.parseDouble(line[i]);
            }
            index++;
        }
        return data;
    }

    private static DataWithClasses getDataWithClasses(String csvFileName) {
        List<String[]> dataLines = parseToLines(csvFileName);
        int dataSize = dataLines.size();
        int vectorDimension = dataLines.iterator().next().length - 1;

        double[][] data = new double[dataSize][vectorDimension];
        int[] classLabels = new int[dataSize];
        Iterator<String[]> iterator = dataLines.iterator();
        int index = 0;
        while (iterator.hasNext()) {
            String[] line = iterator.next();
            for (int i = 0; i < vectorDimension; i++) {
                data[index][i] = Double.parseDouble(line[i]);
            }
            classLabels[index] = Integer.parseInt(line[vectorDimension]);
            index++;
        }
        return new DataWithClasses(data, classLabels);
    }

    private static List<String[]> parseToLines(String csvFileName) {
        File csvFile = new File(csvFileName);
        List<String[]> dataLines = new LinkedList<>();
        try (CSVReader reader = new CSVReader(new FileReader(csvFile), ';', '\"', 1)) {
            String[] line;
            while ((line = reader.readNext()) != null) {
                dataLines.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return dataLines;
    }

    private static class DataWithClasses {
        double[][] data;
        int[] classLabels;

        public DataWithClasses(double[][] data, int[] classLabels) {
            this.data = data;
            this.classLabels = classLabels;
        }
    }
}
