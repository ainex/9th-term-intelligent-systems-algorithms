package edu.ulyanova.kneighbors;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class KNearestNeighborsAlgorithm {
    private final double[][] testSet;
    private final double[][] trainingSet;
    private final int[] trainingClassLabels;
    private final int k;
    private final int vectorDimension;

    private static final Comparator<Neighbour> DISTANCE_COMPARATOR = (Neighbour o1, Neighbour o2) -> Double.compare(o1.getDistance(), o2.getDistance());

    public KNearestNeighborsAlgorithm(double[][] trainingSet, int[] trainingClassLabels, double[][] testSet, int k) {
        this.trainingSet = trainingSet;
        this.trainingClassLabels = trainingClassLabels;
        this.testSet = testSet;
        this.k = k;
        this.vectorDimension = trainingSet[0].length;
    }

    public int[] doClassification() {
        int[] testClassLabels = new int[testSet.length];
        for (int index = 0; index < testSet.length; index++) {
            testClassLabels[index] = defineClass(testSet[index]);
        }
        return testClassLabels;
    }

    private int defineClass(double[] observation) {
        List<Neighbour> neighbours = defineNeighbors(observation);
        neighbours.sort(DISTANCE_COMPARATOR);
        Map<Integer, Integer> nearestNeighbors = neighbours.stream().limit(k).collect(Collectors.toMap(Neighbour::getClassLabel, n -> 1, (existing, replacement) -> existing + 1));
        Map.Entry<Integer, Integer> classLabels = nearestNeighbors.entrySet().stream()
                .max(Map.Entry.comparingByValue()).get();
        return classLabels.getKey();
    }

    private List<Neighbour> defineNeighbors(double[] observation) {
        List<Neighbour> neighbours = new ArrayList<>();

        for (int index = 0; index < trainingSet.length; index++) {
            double sumOfSquares = 0;
            for (int i = 0; i < vectorDimension; i++) {
                sumOfSquares += Math.pow(observation[i] - trainingSet[index][i], 2);
            }
            double distance = Math.sqrt(sumOfSquares);
            neighbours.add(new Neighbour(index, distance, trainingClassLabels[index]));
        }

        return neighbours;
    }
}
