package edu.ulyanova.kneighbors;

public class Neighbour {
    private int index;
    private double distance;
    private int classLabel;

    public Neighbour(int index, double distance, int classLabel) {
        this.index = index;
        this.distance = distance;
        this.classLabel = classLabel;
    }

    public int getIndex() {
        return index;
    }

    public double getDistance() {
        return distance;
    }

    public int getClassLabel() {
        return classLabel;
    }
}
